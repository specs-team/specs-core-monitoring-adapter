
This component is useful to send the measured events to the event hub.
The events data are grouped into a formatted json.

To send an event you have to execute the following steps:

* Creation of the json event

```
#!java
AdapterImpl impl = new AdapterImpl();
String eventJson = impl.generateJsonEvent("", "component", true, "slaid", "metricid", null);
```

* Sending the json

```
#!java
AdapterImpl impl = new AdapterImpl();
impl.sendJsonEvent(eventJson, "ip_address", port_number);
```