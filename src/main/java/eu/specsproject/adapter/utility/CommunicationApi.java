package eu.specsproject.adapter.utility;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public class CommunicationApi {



	public static void sendUdpPacket(String ipAddress, int port, String data) throws IOException{
		DatagramSocket socket = new DatagramSocket();
		byte[] buf = new byte[256];
		buf = data.getBytes();
		InetAddress address = InetAddress.getByName(ipAddress);
		DatagramPacket packet = new DatagramPacket(buf, buf.length, address, port);
		socket.send(packet);
		socket.close();
	}

	public static void sendTcpPacket(String ipAddress, int port, String data, int timeout) throws IOException{
		SocketAddress address = new InetSocketAddress(ipAddress, port);
		Socket clientSocket = new Socket();
		clientSocket.connect(address, timeout); 
		DataOutputStream outToServer = new DataOutputStream(clientSocket.getOutputStream());
		outToServer.writeBytes(data+'\n');
		clientSocket.close();
	}

	public static void sendPacketWithPost(String ipAddress, String data){
		Client client = ClientBuilder.newClient();
		WebTarget target = client.target(ipAddress);
		System.out.println("Send Package request");		
		try{
			Response response = target.request()
					.post(Entity.entity(data,MediaType.APPLICATION_JSON));
			System.out.println("Send Package result: "+response.getStatus());		
		}catch(Exception e){
			System.out.println("Send Exception: "+e.getMessage());
		}

	}


}
