package eu.specsproject.adapter.entities;

public enum Type {
	INTEGER,
	BOOLEAN,
	STRING
}
