package eu.specsproject.adapter.entities;

import java.util.List;

public class Event {
	private String object;
	private Float timestamp;
	private List<String> labels;
	private String component;
	private String token;
	private String type;
	private String data;

	public Event(){}

	public Event(String object,
			Float timestamp,
			List<String> labels,
			String component,
			String token,
			String type,
			String data){

		this.object = object;
		this.timestamp = timestamp;
		this.labels = labels;
		this.component = component;
		this.type = type;
		this.data = data;

	}


	public String getObject() {
		return object;
	}
	public void setObject(String object) {
		this.object = object;
	}
	public Float getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(Float timestamp) {
		this.timestamp = timestamp;
	}
	public List<String> getLabels() {
		return labels;
	}
	public void setLabels(List<String> labels) {
		this.labels = labels;
	}
	public String getComponent() {
		return component;
	}
	public void setComponent(String component) {
		this.component = component;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}

}
