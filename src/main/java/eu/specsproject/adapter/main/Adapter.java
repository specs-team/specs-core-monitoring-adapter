package eu.specsproject.adapter.main;

import java.util.ArrayList;

public interface Adapter {
	
	/**
     * This method generate a Monitoring Event in JSON format to send it to the Event Hub
     * 
     * 
     * @param object - the object value (it can be empty)
     * @param component - the component_id field of the implementation plan
     * @param data - boolean value of the measurement
     * @param slaId - the sla id value     
     * @param securityMetric - the id of the measured security metric
     * @param labels - other label values (it can be null)
     * 
     * @output String that represents the json format of the event
     */
	public String generateJsonEvent(String object, String component, 
			Boolean data, String slaId, String securityMetric, ArrayList<String> labels);

	/**
     * This method generate a Monitoring Event in JSON format to send it to the Event Hub
     * 
     * 
     * @param object - the object value (it can be empty)
     * @param component - the component_id field of the implementation plan
     * @param data - integer value of the measurement
     * @param slaId - the sla id value     
     * @param securityMetric - the id of the measured security metric
     * @param labels - other label values (it can be null)
     * 
     * @output String that represents the json format of the event
     */
	public String generateJsonEvent(String object, String component, 
			Integer data, String slaId, String securityMetric, ArrayList<String> labels);

	/**
     * This method generate a Monitoring Event in JSON format to send it to the Event Hub
     * 
     * 
     * @param object - the object value (it can be empty)
     * @param component - the component_id field of the implementation plan
     * @param data - string value of the measurement
     * @param slaId - the sla id value     
     * @param securityMetric - the id of the measured security metric
     * @param labels - other label values (it can be null)
     * 
     * @output String that represents the json format of the event
     */
	public String generateJsonEvent(String object, String component, 
			String data, String slaId, String securityMetric, ArrayList<String> labels);

	
	/**
     * This method generate a Monitoring Event in JSON format to send it to the Event Hub
     * 
     * 
     * @param jsonEvent - the json representation of the event
     * @param ipAddress - the ip address of the event hub
     * @param component - the port number of the event hub
     * 
     * @output Boolean value that represents the result of the submission
     */
	public Boolean sendJsonEvent(String jsonEvent, String ipAddress, int portNumber);

}
