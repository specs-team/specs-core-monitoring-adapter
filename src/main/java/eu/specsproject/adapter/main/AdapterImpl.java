package eu.specsproject.adapter.main;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;

import eu.specsproject.adapter.entities.Event;
import eu.specsproject.adapter.entities.Type;
import eu.specsproject.adapter.utility.CommunicationApi;

public class AdapterImpl implements Adapter{

	@Override
	public String generateJsonEvent(String object,
			String component, Boolean data, String slaId,
			String securityMetric, ArrayList<String> labels) {
		
		Event event = new Event();
		event.setObject("");
		event.setTimestamp(Float.valueOf(System.currentTimeMillis()/1000));
		event.setComponent(component);
		event.setType(Type.BOOLEAN.toString().toLowerCase());
		event.setData(data.toString().toLowerCase());
		if(labels != null){
			labels.add("sla_id_"+slaId);
			labels.add("security_metric_"+securityMetric);
		}else{
			labels = new ArrayList<String>();
			labels.add("sla_id_"+slaId);
			labels.add("security_metric_"+securityMetric);
		}
		event.setLabels(labels);

		return new Gson().toJson(event);
	}

	@Override
	public String generateJsonEvent(String object,
			String component, Integer data, String slaId,
			String securityMetric, ArrayList<String> labels) {
		
		Event event = new Event();
		event.setObject("");
		event.setTimestamp(Float.valueOf(System.currentTimeMillis()/1000));
		event.setComponent(component);
		event.setType(Type.INTEGER.toString().toLowerCase());
		event.setData(data.toString().toLowerCase());
		if(labels != null){
			labels.add("sla_id_"+slaId);
			labels.add("security_metric_"+securityMetric);
		}else{
			labels = new ArrayList<String>();
			labels.add("sla_id_"+slaId);
			labels.add("security_metric_"+securityMetric);
		}
		event.setLabels(labels);

		return new Gson().toJson(event);
	}
	
	@Override
	public String generateJsonEvent(String object,
			String component, String data, String slaId,
			String securityMetric, ArrayList<String> labels) {
		
		Event event = new Event();
		event.setObject("");
		event.setTimestamp(Float.valueOf(System.currentTimeMillis()/1000));
		event.setComponent(component);
		event.setType(Type.STRING.toString().toLowerCase());
		event.setData(data);
		if(labels != null){
			labels.add("sla_id_"+slaId);
			labels.add("security_metric_"+securityMetric);
		}else{
			labels = new ArrayList<String>();
			labels.add("sla_id_"+slaId);
			labels.add("security_metric_"+securityMetric);
		}
		event.setLabels(labels);

		return new Gson().toJson(event);
	}
	
	@Override
	public Boolean sendJsonEvent(String jsonEvent, String ipAddress,
			int portNumber) {
		ipAddress = ipAddress.startsWith("http://") ? ipAddress : "http://"+ipAddress;

		String path = ipAddress + ":" + portNumber + "/events/event";
		System.out.println("Send packet with post api");
		CommunicationApi.sendPacketWithPost(path, jsonEvent);
		return true;
	}


	
}
